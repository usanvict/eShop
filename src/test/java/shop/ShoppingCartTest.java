package shop;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.*;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ShoppingCartTest {
    @Test
    public void constructorTest(){
        //setup
        ShoppingCart shopCart = new ShoppingCart();
        //assert
        assertNotNull(shopCart);
    }

    @Test
    public void howManyItemsAreInTheCart_noItemSupposedToBe(){
        //setup
        ShoppingCart shopCart = new ShoppingCart();
        ArrayList<Item> actual = shopCart.getCartItems();
        assertEquals(0, actual.size());
        //act
        shopCart.getCartItems();
    }

    @Test
    public void howManyItemsInTheCart_oneItemIsSupposedToBe(){
        //setup
        ShoppingCart shopCart = new ShoppingCart();
        Item item = new StandardItem(1, "name", 3.14f,"category",10);
        //act
        shopCart.addItem(item);
        int actual = shopCart.getItemsCount();
        //assert
        assertEquals(1, actual);
    }

    @Test
    public void whatIsThePriceOfCart_20(){
        //setup
        ShoppingCart shopCart = new ShoppingCart();
        Item item1 = new StandardItem(1, "name1", 1,"category",10);
        Item item2 = new StandardItem(2, "name2", 2,"category",20);
        Item item3 = new StandardItem(3, "name3", 17,"category",30);
        //act
        shopCart.addItem(item1);
        shopCart.addItem(item2);
        shopCart.addItem(item3);
        //BYLO POTREBA UPRAVIT PUVODNI FUNKCI getTotalPrice(), JINAK NEFUNGOVALA A HAZELA CHYBY
        int price = shopCart.getTotalPrice();
        //assert
        assertEquals(20,price);
    }

    @Test
    public void isItemRemoved_2(){
        //setup
        ShoppingCart shopCart = new ShoppingCart();
        Item item1 = new StandardItem(1, "name1", 1,"category",10);
        Item item2 = new StandardItem(2, "name2", 2,"category",20);
        Item item3 = new StandardItem(3, "name3", 17,"category",30);
        //act
        shopCart.addItem(item1);
        shopCart.addItem(item2);
        shopCart.addItem(item3);
        int numberOfItems1 = shopCart.getItemsCount();
        //assert
        assertEquals(3,numberOfItems1);
        //act
        //BYLO POTREBA UPRAVIT PUVODNI FUNKCI removeItem(), JINAK NEFUNGOVALA A HAZELA CHYBY
        shopCart.removeItem(2);
        //assert
        int numberOfItems2 = shopCart.getItemsCount();
        assertEquals(2,numberOfItems2);
    }
}
